import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;

// full branch coverage for function UserList
class forumDAOTests {
  JdbcTemplate jdbcMock;
  ForumDAO forum;

  @BeforeEach
  void init() {
    jdbcMock = Mockito.mock(JdbcTemplate.class);
    forum = new ForumDAO(jdbcMock);
  }

  private static Stream<Arguments> provideArgs() {
    return Stream.of(
        Arguments.of("slug", 1, "since", true,
            "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
        Arguments.of("slug", null, "since", true,
            "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
        Arguments.of("slug", 1, "since", true,
            "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
        Arguments.of("slug", null, "since", null,
            "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
        Arguments.of("slug", 1, null, true,
            "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"),
        Arguments.of("slug", null, null, true,
            "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc;"),
        Arguments.of("slug", 1, null, null,
            "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"),
        Arguments.of("slug", null, null, null,
            "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"));
  }

  @ParameterizedTest
  @MethodSource("provideArgs")
  void TestUserList(String slug, Integer limit, String since, Boolean desc, String expected) {
    ForumDAO.UserList(slug, limit, since, desc);
    Mockito.verify(jdbcMock).query(Mockito.eq(expected), Mockito.any(Object[].class),
        Mockito.any(UserDAO.UserMapper.class));
  }
}
